terraform {
  backend "s3" {}
}
provider "aws" {
    region     = var.data["region"]
}
module "sg" {
    source          = "./modules/sg"
    name            = "${local.app_name}-app-sg"
    vpc_id          = var.vpc_id
    source_sg_id    = module.alb_sg.sec_grp_id
    sg_id           = module.sg.sec_grp_id
    tags            = merge(local.common_tags,map("Name","${local.app_name}-app-sg",))
}
module "alb_sg" {
    source          = "./modules/sg_alb"
    name            = "${local.app_name}-alb-sg"
    sg_id           = module.alb_sg.sec_grp_id
    vpc_id          = var.vpc_id
    tags            = merge(local.common_tags,map("Name","${local.app_name}-alb-sg",))
}
module "lc_sonar" {
    source          = "./modules/launch_template"
    name            = "${local.app_name}-app"
    ami_id          = var.ami_id
    ec2_type        = "t2.medium"
    key_name        = var.data["key_name"]
    sg_id           = module.sg.sec_grp_id
    tags            = merge(local.common_tags,map("Name","${local.app_name}-app", "Role","sonar-app"))
}
module "lc_search" {
    source          = "./modules/launch_template"
    name            = "${local.app_name}-search"
    ami_id          = var.ami_id
    ec2_type        = "t2.large"
    key_name        = var.data["key_name"]
    sg_id           = module.sg.sec_grp_id
    tags            = merge(local.common_tags,map("Name","${local.app_name}-search", "Role","sonar-search"))
}
module "asg_sonar" {
    source          = "./modules/asg"
    name            = "${local.app_name}-app-asg"
    max_size        = "5"
    min_size        = "2"
    desired_size    = "2"
    target_arn      = module.alb.target_arn
    launch_config   = module.lc_sonar.lc_id
    subnet_ids      = var.subnet_ids
    tags            = concat(list(map("key", "Name", "value", "${local.app_name}-app", "propagate_at_launch", true),
                          map("key", "Role", "value", "sonar-app", "propagate_at_launch", true)
                          ),local.asg_tags)
}
module "asg_search" {
    source          = "./modules/asg"
    name            = "${local.app_name}-es-asg"
    max_size        = "5"
    min_size        = "3"
    desired_size    = "3"
    launch_config   = module.lc_search.lc_id
    subnet_ids      = var.subnet_ids
    tags            = concat(list(map("key", "Name", "value", "${local.app_name}-search", "propagate_at_launch", true),
                          map("key", "Role", "value", "sonar-search", "propagate_at_launch", true)
                          ),local.asg_tags)
}
module "alb" {
    source          = "./modules/alb"
    name            = local.app_name
    vpc_id          = var.vpc_id
    sg_id           = module.alb_sg.sec_grp_id
    subnet_ids      = var.subnet_ids
    ssl_arn         = var.data["ssl_arn"]
    tags            = merge(local.common_tags,map("Name","${local.app_name}-alb",))
}
/*
module "rds" {
    source          = "./modules/rds"
    name            = local.app_name
    subnet_ids      = var.subnet_ids
    storage         = "10"
    storage_type    = "gp2"
    engine          = "postgres"
    engine_version  = "10.10"
    sg_id           = module.sg.sec_grp_id
    instance_class  = "db.t2.micro"
    username        = var.data["dbuser"]
    password        = var.data["dbpassword"]
    tags            = merge(local.common_tags,map("Name","${local.app_name}-rds", "Role","sonar-rds"))
}
module "lambda" {
    source          = "./modules/lambda"
    name            = local.app_name
    region          = var.data["region"]
    sg_id           = module.sg.sec_grp_id
    subnet_ids      = var.subnet_ids
    vpc_id          = var.vpc_id
    db_name         = module.rds.db_name
    db_endpoint     = module.rds.db_endpoint
    db_username     = module.rds.db_username
    db_password     = module.rds.db_password
    sonar_asg_name  = module.asg_sonar.asg_grp_name
    search_asg_name = module.asg_search.asg_grp_name
    p_key           = "${var.data["p_key"]}"
    tags            = "${merge(local.common_tags)}"
}
module "secret" {
    source          = "./modules/outputs"
    region          = var.data["region"]
    db_name         = module.rds.db_name
    db_endpoint     = module.rds.db_endpoint
    db_username     = module.rds.db_username
    db_password     = module.rds.db_password
    sonar_asg_name  = module.asg_sonar.asg_grp_name
    search_asg_name = module.asg_search.asg_grp_name
    lambda_name     = "${local.app_name}-lambda"
}
*/
module "route53" {
    source          = "./modules/route53"
    domain_name     = "sonar.development.com"
    zone_id         = "Z08323433D8YJXE069OYB"
    alb_zone_id     = module.alb.alb_zone_id
    alb_dns_name    = module.alb.alb_dns_name
}