variable "name" {
  type        = string
  default     = ""
}
variable "launch_config" {
  type        = string
  default     = ""
}
variable "subnet_ids" {
  type        = string
  default     = ""
}
variable "target_arn" {
  type        = string
  default     = ""
}
variable "max_size" {
  type        = string
  default     = ""
}
variable "min_size" {
  type        = string
  default     = ""
}
variable "desired_size" {
  type        = string
  default     = ""
}
variable "tags" {
  description = "A mapping of tags to assign to the resource"
  type        = list
  default     = []
}
