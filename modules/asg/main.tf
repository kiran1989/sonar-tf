resource "aws_autoscaling_group" "t-asg" {
  name                      = var.name
  max_size                  = var.max_size
  min_size                  = var.min_size
  health_check_grace_period = 120
  default_cooldown          = 300
  health_check_type         = "EC2"
  desired_capacity          = var.desired_size
  force_delete              = true
  vpc_zone_identifier       = [var.subnet_ids]
  target_group_arns         = [var.target_arn]
  termination_policies      = ["NewestInstance"]
  enabled_metrics = ["GroupMinSize","GroupMaxSize","GroupDesiredCapacity","GroupInServiceInstances","GroupPendingInstances","GroupStandbyInstances","GroupTerminatingInstances","GroupTotalInstances"]
  
  launch_template {
      id      = var.launch_config
      version = "$Latest"
    }

  tags = var.tags
}

resource "aws_autoscaling_policy" "t-scale-up" {
  name                   = "${var.name}-scale-up"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.t-asg.name
}

resource "aws_cloudwatch_metric_alarm" "scale-up-alarm" {
  alarm_name          = "${var.name}-scale-up-alarm"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "300"
  statistic           = "Average"
  threshold           = "80"
  insufficient_data_actions = []

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.t-asg.name
  }

  alarm_description = "${var.name} ec2 high cpu utilization"
  alarm_actions     = [aws_autoscaling_policy.t-scale-up.arn]
}


resource "aws_autoscaling_policy" "t-scale-down" {
  name                   = "${var.name}-scale-down"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.t-asg.name
}

resource "aws_cloudwatch_metric_alarm" "scale-down-alarm" {
  alarm_name          = "${var.name}-scale-down-alarm"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "300"
  statistic           = "Average"
  threshold           = "30"
  insufficient_data_actions = []

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.t-asg.name
  }

  alarm_description = "${var.name} ec2 less cpu utilization"
  alarm_actions     = [aws_autoscaling_policy.t-scale-down.arn]
}