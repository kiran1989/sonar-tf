resource "aws_security_group" "t-sec-grp" {
  name        = var.name
  description = "Security Group for ${var.name} sonar and search applications"
  vpc_id      = var.vpc_id

  ingress {
      from_port = 0
      to_port   = 0
      protocol  = -1
      self      = true
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  
tags = merge(
    var.tags,
    { },
  )
}

resource "aws_security_group_rule" "sonar_allow_all" {
  type            = "ingress"
  from_port       = 9000
  to_port         = 9000
  protocol        = "tcp"
  source_security_group_id  = var.source_sg_id  
  security_group_id = var.sg_id
  description = "Allow https port 9000 from alb-${var.source_sg_id} security group for sonar app"
}

resource "aws_security_group_rule" "ssh_allow_all" {
  type            = "ingress"
  from_port       = 22
  to_port         = 22
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]  
  security_group_id = var.sg_id
  description = "Allow ssh from private subnet"
}