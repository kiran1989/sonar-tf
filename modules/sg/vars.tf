variable "name" {
  type        = string
  default     = ""
}
variable "vpc_id" {
  type        = string
  default     = ""
}
variable "sg_id" {
  type        = string
  default     = ""
}
variable "source_sg_id" {
  type        = string
  default     = ""
}
variable "tags" {
  description = "A mapping of tags to assign to the resource"
  type        = map(string)
  default     = {}
}