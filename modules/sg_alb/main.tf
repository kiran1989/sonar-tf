resource "aws_security_group" "t-sec-grp" {
  name        = var.name
  description = "Security Group for ${var.name} alb"
  vpc_id      = var.vpc_id

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  
tags = merge(
    var.tags,
    { },
  )
}

resource "aws_security_group_rule" "http_allow_all" {
  type            = "ingress"
  from_port       = 80
  to_port         = 80
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]  
  security_group_id = var.sg_id
  description = "Allow http to public"
}

resource "aws_security_group_rule" "https_allow_all" {
  type            = "ingress"
  from_port       = 443
  to_port         = 443
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]  
  security_group_id = var.sg_id
  description = "Allow http to public"
}