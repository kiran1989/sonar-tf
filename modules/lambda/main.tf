resource "aws_iam_role" "t-lambda-iam-role" {
  name = "${var.name}-iam-lambda-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "t-lambda_logging" {
  name = "${var.name}-lambda-policy"
  path = "/"
  description = "IAM policy for ${var.name} logging from a lambda"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "CreateLogStream",
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "arn:aws:logs:${var.region}:*:log-group:/aws/lambda/${var.name}-lambda:*"
        },
        {
            "Sid": "CreateLogGroup",
            "Effect": "Allow",
            "Action": "logs:CreateLogGroup",
            "Resource": "arn:aws:logs:${var.region}:*:*"
        },
        {   
            "Sid": "CreateNetworkVPC",
            "Effect": "Allow",
            "Action": [
              "ec2:CreateNetworkInterface",
              "ec2:DescribeNetworkInterfaces",
              "ec2:DeleteNetworkInterface"
            ],
            "Resource": "*"
        },
        {   
            "Sid": "DescribeAsgEc2",
            "Effect": "Allow",
            "Action": [
              "autoscaling:DescribeAutoScalingGroups",
              "ec2:DescribeInstances"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "t-lambda_logs" {
  role = aws_iam_role.t-lambda-iam-role.name
  policy_arn = aws_iam_policy.t-lambda_logging.arn
}

resource "aws_lambda_function" "t-lambda" {
  function_name = "${var.name}-lambda"
  role          = aws_iam_role.t-lambda-iam-role.arn
  handler       = "lambda_function.lambda_handler"
  runtime       = "python2.7"
  timeout       = "600"
  filename      = "lambda.zip"
  source_code_hash = data.archive_file.t-files.output_base64sha256

  layers = [aws_lambda_layer_version.t-lambda-layer.arn]
/*
  vpc_config {
  subnet_ids         = split(",", "${var.subnet_ids}")
  security_group_ids = ["${var.sg_id}"]
  }
  */
  environment {
    variables = {
      REGION = var.region
      ASG = var.sonar_asg_name
    }
  }
   
  tags = merge(
    var.tags,
    map("Name", "${var.name}-lambda"),
  )
}

resource "aws_lambda_layer_version" "t-lambda-layer" {
  filename   = "lambda_function/paramiko.zip"
  layer_name = "${var.name}-paramiko"

  compatible_runtimes = ["python2.7"]
}

resource "aws_cloudwatch_event_rule" "t-cw-rule" {
  name        = "${var.name}-asg-rule"
  description = "Capture all EC2 scaling events from ${var.name} asg"

  event_pattern = <<PATTERN
	{
  "source": [
    "aws.autoscaling"
  ],
  "detail-type": [
    "EC2 Instance Launch Successful",
    "EC2 Instance Terminate Successful"
  ],
  "detail": {
    "AutoScalingGroupName": [
      "${var.sonar_asg_name}"
    ]
  }
}
PATTERN
}

resource "aws_cloudwatch_event_target" "t-cw-target" {
  target_id = "${var.name}-cw-taget"
  rule      = aws_cloudwatch_event_rule.t-cw-rule.name
  arn       = aws_lambda_function.t-lambda.arn
}

resource "aws_lambda_permission" "allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.t-lambda.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.t-cw-rule.arn
}

data "local_file" "t-file-read" {
    filename = "lambda_function/lambda_function.py"
}

data "archive_file" "t-files" {
  type        = "zip"
  output_path = "lambda.zip"

  source {
    content  = data.local_file.t-file-read.content
    filename = "lambda_function.py"
  }

  source {
    content = jsonencode({"REGION"="${var.region}","SONAR_ASG"="${var.sonar_asg_name}","SEARCH_ASG"="${var.search_asg_name}","DB_ENDPOINT"="${var.db_endpoint}","DB_NAME"="${var.db_name}","DB_USER"="${var.db_username}","DB_PASSWORD"="${var.db_password}","PRIVATE_KEY"="${var.p_key}"})
    filename = "config.json"
  }
}