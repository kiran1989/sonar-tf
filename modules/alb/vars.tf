variable "name" {
  type        = string
  default     = ""
}
variable "vpc_id" {
  type        = string
  default     = ""
}
variable "sg_id" {
  type        = string
  default     = ""
}
variable "asg_name" {
  type        = string
  default     = ""
}
variable "subnet_ids" {
    type = string
    default     = ""
}
variable "ssl_arn" {
    type = string
    default     = ""
}
variable "tags" {
  type        = map(string)
  default     = {}
}