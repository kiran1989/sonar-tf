output "target_arn" {
value = "${aws_lb_target_group.t-targetgroup.arn}"
} 
output "alb_dns_name" {
value = "${aws_lb.t-alb.dns_name}"
} 
output "alb_zone_id" {
value = "${aws_lb.t-alb.zone_id}"
} 
