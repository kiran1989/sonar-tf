resource "aws_lb" "t-alb" {
  name               = "${var.name}-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [var.sg_id]
  subnets            = split(",", "${var.subnet_ids}")

  enable_deletion_protection = false

tags = merge(
    var.tags,
    { },
  )
}

resource "aws_lb_target_group" "t-targetgroup" {
  name        = "${var.name}-tg"
  port        = 9000
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = "instance"
  
  stickiness {    
    type            = "lb_cookie"    
    cookie_duration = 1800    
    enabled         = "false" 
  }   
  
  health_check {
    interval            = 30
    path                = "/"
    port                = 9000
    healthy_threshold   = 5
    unhealthy_threshold = 2
    timeout             = 5
    protocol            = "HTTP"
    matcher             = "200,202"
  }
  
  tags = merge(
    var.tags,
    map("Name", "${var.name}-tg"),
  )
}

resource "aws_lb_listener" "t-alb-https-listener" {
  load_balancer_arn = aws_lb.t-alb.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = var.ssl_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.t-targetgroup.arn
  }
}

resource "aws_lb_listener" "t-alb-http-listener" {
  load_balancer_arn = aws_lb.t-alb.arn
  port              = "80"
  protocol          = "HTTP"

 default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}