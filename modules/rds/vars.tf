variable "name" {
  type        = string
  default     = ""
}
variable "storage" {
  type        = string
  default     = ""
}
variable "storage_type" {
  type        = string
  default     = ""
}
variable "engine" {
  type        = string
  default     = ""
}
variable "engine_version" {
  type        = string
  default     = ""
}
variable "sg_id" {
  type        = string
  default     = ""
}
variable "subnet_ids" {
  type        = string
  default     = ""
}
variable "instance_class" {
    type = string
    default     = ""
}
variable "username" {
    type = string
    default     = ""
}
variable "password" {
    type = string
    default     = ""
}
variable "tags" {
  type        = map(string)
  default     = {}
}
