resource "aws_db_instance" "t-rds" {
  name                 = "sonarqube"
  identifier           = lower("${var.name}-rds")
  allocated_storage    = var.storage
  storage_type         = var.storage_type
  engine               = var.engine
  engine_version       = var.engine_version
  instance_class       = var.instance_class
  username             = var.username
  password             = var.password
  copy_tags_to_snapshot = true
  parameter_group_name = ""
  deletion_protection = false
  publicly_accessible = false
  multi_az = true
  vpc_security_group_ids = [var.sg_id]
  db_subnet_group_name = aws_db_subnet_group.t-rdssubnet.name
  auto_minor_version_upgrade = true
  maintenance_window = "Sun:00:00-Sun:03:00"
  skip_final_snapshot = true

  tags = merge(
    var.tags,
    { },
  )

}

resource "aws_db_subnet_group" "t-rdssubnet" {
  name       = lower("${var.name}-sub-grp")
  description = "Managed by ${var.name}"
  subnet_ids = split(",", "${var.subnet_ids}")

  tags = merge(
    var.tags,
    map("Name", "${var.name}-sub-grp"),
  )
}
