output "db_name" {
  value       = aws_db_instance.t-rds.name
  sensitive   = false
}
output "db_password" {
  value       = aws_db_instance.t-rds.password
  sensitive   = false
}
output "db_username" {
  value       = aws_db_instance.t-rds.username
}
output "db_endpoint" {
  value       = aws_db_instance.t-rds.endpoint
}