variable "zone_id" {
  type        = string
  default     = ""
}
variable "domain_name" {
  type        = string
  default     = ""
}
variable "alb_dns_name" {
  type        = string
  default     = ""
}
variable "alb_zone_id" {
  type        = string
  default     = ""
}
