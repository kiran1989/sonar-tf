resource "local_file" "db-secrets-out" {
    content = jsonencode({"REGION"="${var.region}","SONAR_ASG"="${var.sonar_asg_name}","SEARCH_ASG"="${var.search_asg_name}","DB_ENDPOINT"="${var.db_endpoint}","DB_NAME"="${var.db_name}","DB_USER"="${var.db_username}","DB_PASSWORD"="${var.db_password}","LAMBDA_NAME"="${var.lambda_name}"})
    filename = "config.json"
}