variable "name" {
  type        = string
  default     = ""
}
variable "db_name" {
  type        = string
  default     = ""
}
variable "db_endpoint" {
  type        = string
  default     = ""
}
variable "db_username" {
  type        = string
  default     = ""
}
variable "db_password" {
  type        = string
  default     = ""
}
variable "sonar_asg_name" {
  type        = string
  default     = ""
}
variable "search_asg_name" {
  type        = string
  default     = ""
}
variable "region" {
  type        = string
  default     = ""
}
variable "lambda_name" {
  type        = string
  default     = ""
}
variable "tags" {
  description = "A mapping of tags to assign to the resource"
  type        = map(string)
  default     = {}
}