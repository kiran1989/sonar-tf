resource "aws_launch_template" "t-launch-template" {
  name          = var.name
  image_id      = var.ami_id
  instance_type = var.ec2_type
  key_name      = var.key_name
  vpc_security_group_ids = [var.sg_id]

  monitoring {
    enabled = true
  }

  tag_specifications {
    resource_type = "instance"
    tags = merge(var.tags,{},)
  }
  tag_specifications {
    resource_type = "volume"
    tags = merge(var.tags,{},)
  }
  tags = merge(
    var.tags,
    map("Name", "${var.name}-lt"),
  )
}

