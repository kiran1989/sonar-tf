variable "name" {
  type        = string
  default     = ""
}
variable "ami_id" {
  type        = string
  default     = ""
}

variable "ec2_type" {
  type        = string
  default     = "t2.micro"
}
variable "key_name" {
  type        = string
  default     = ""
}
variable "sg_id" {
  type        = string
  default     = ""
}
variable "tags" {
  type        = map(string)
  default     = {}
}