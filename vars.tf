variable "data" {
    type = map(string)
}
variable "vpc_id" {
    default = "vpc-b7c729dc"
}

variable "subnet_ids" {
   default = "subnet-13b2fe69,subnet-1ee35452"
 }

variable "ami_id" {
    default = ""
}

variable "env_prefix" {
    default = {
    "staging" = "stg" 
    "production" = "prd"
    }
}

locals {
  app_name = "${lower("${var.env_prefix[var.data["env"]]}-${lower("${var.data["app"]}")}-${substr("${var.data["name"]}", 0, 3)}")}"
}

locals {
    common_tags = "${map(
        "Application", "${var.data["app"]}",
        "Name","${local.app_name}",
        "Owner", "${var.data["owner"]}",
        "Stack", "${var.data["name"]}",
        "Role","${var.data["name"]}-app",
        "Environment", "${var.data["env"]}",
        "Workload","${lower("${var.data["app"]}")}-${var.data["name"]}"
    )}"
}

locals {
    asg_tags = "${list(map("key","Application","value","${var.data["app"]}","propagate_at_launch","true"),
        map("key","Owner","value","${var.data["owner"]}","propagate_at_launch","true"),
        map("key","Stack","value","${var.data["name"]}","propagate_at_launch","true"),
        map("key","Role","value","${var.data["name"]}-app","propagate_at_launch","true"),
        map("key","Environment","value","${var.data["env"]}","propagate_at_launch","true"),
        map("key","Workload","value","${lower("${var.data["app"]}")}-${var.data["name"]}","propagate_at_launch","true")
    )}"
}