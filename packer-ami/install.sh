#/bin/bash
# set -e
sudo yum update -y
#sudo mkfs -t xfs /dev/xvdb
#sudo lsblk
#sudo mkdir /app
#sudo mount /dev/xvdb /app/
#export UUID="`/sbin/blkid -s UUID -o value /dev/xvdb`"
#sudo sed -i '$ a\UUID='`$UUID`' /app                    xfs     defaults,nofail 0 2' /etc/fstab
#sleep 5
#sudo sed -i '$ a\UUID='`sudo /sbin/blkid -s UUID -o value /dev/xvdb`' /app                    xfs     defaults,nofail 0 2' /etc/fstab
#cat /etc/fstab
sudo yum install java-11-openjdk-devel unzip wget -y
sudo wget https://binaries.sonarsource.com/CommercialDistribution/sonarqube-datacenter/sonarqube-datacenter-7.9.1.zip
sudo unzip sonarqube-datacenter-7.9.1.zip && sudo mv sonarqube-7.9.1 /srv/sonarqube
sudo cp /tmp/sonar /etc/init.d/sonar
sudo ln -s /srv/sonarqube/bin/linux-x86-64/sonar.sh /usr/bin/sonar
sudo chmod 755 /etc/init.d/sonar
sudo chkconfig --add sonar