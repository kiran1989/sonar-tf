import os
import boto3
import time
import json
import paramiko
import base64
import StringIO

with open('config.json') as config_data:
            data = json.load(config_data)
            
region = data['REGION']
sonar_asg_name = data['SONAR_ASG']
search_asg_name = data['SEARCH_ASG']
asg_client = boto3.client('autoscaling',region_name=region)
ec2_client = boto3.client('ec2',region_name=region)
template = '/srv/sonarqube/conf/sonar.properties'

def lambda_handler(event, context):
            
    def get_asg_instance_id():
        instance_ids = []
        asg_response = asg_client.describe_auto_scaling_groups(AutoScalingGroupNames=[sonar_asg_name])
        for i in asg_response['AutoScalingGroups']:
            for k in i['Instances']:
                instance_ids.append(k['InstanceId'])
        return (instance_ids)

    def get_search_instance_ips():
        instance_ids = []
        instance_ips = []
        asg_response = asg_client.describe_auto_scaling_groups(AutoScalingGroupNames=[search_asg_name])
        for i in asg_response['AutoScalingGroups']:
            for k in i['Instances']:
                instance_ids.append(k['InstanceId'])
        ec2_response = ec2_client.describe_instances(InstanceIds = instance_ids)
        for instances in ec2_response['Reservations']:
            for x in instances['Instances']:
                instance_ips.append(x['PrivateIpAddress'])
        return (instance_ips)
        
    def get_asg_instance_health():
        instance_health = []
        asg_response = asg_client.describe_auto_scaling_groups(AutoScalingGroupNames=[sonar_asg_name])
        for i in asg_response['AutoScalingGroups']:
            for k in i['Instances']:
                instance_health.append(k['HealthStatus'])
        return (instance_health)
    
    def get_asg_instance_status():
        instance_status = []
        asg_response = asg_client.describe_auto_scaling_groups(AutoScalingGroupNames=[sonar_asg_name])
        for i in asg_response['AutoScalingGroups']:
            for k in i['Instances']:
                instance_status.append(k['LifecycleState'])
        return (instance_status)
    
    def get_ec2_status(x):
        ec2_status = []
        instance_ids = x
        ec2_response = ec2_client.describe_instances(InstanceIds = instance_ids)        
        for instances in ec2_response['Reservations']:
            for x in instances['Instances']:
                ec2_status.append(x['State']['Name'])
        return (ec2_status)
    
    def get_ec2_instance_ips(x):
        instance_ip = []
        instance_ids = x
        ec2_response = ec2_client.describe_instances(InstanceIds = instance_ids)
        for instances in ec2_response['Reservations']:
            for x in instances['Instances']:
                instance_ip.append(x['PrivateIpAddress'])
        return (instance_ip)
        
    def config(x, y):
        ips = x
        es_ips = y
        key = base64.b64decode(data['PRIVATE_KEY'])
        k = paramiko.RSAKey.from_private_key(StringIO.StringIO(key))
        c = paramiko.SSHClient()
        c.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        
        for ip in ips:
            host = ip
            sonar_ips = (",".join(ips))
            search_ips = (",".join(es_ips))
            print "Connecting to " + host
            c.connect( hostname = host, username = "centos", pkey = k )
            print "Connected to " + host
            commands = [
                "sudo adduser sonar ",
                "sudo mv /srv/sonarqube/conf/sonar.properties /srv/sonarqube/conf/sonar_properties_bkp",
                "sudo cp /tmp/sonar_template " + template + "",
                "sudo sed -i 's|DB_USERNAME|" + data['DB_USER'] + "|g' " + template + "",
                "sudo sed -i 's|DB_PASSWORD|" + data['DB_PASSWORD'] + "|g' " + template + "",
                "sudo sed -i 's|DB_URL|jdbc:postgresql://" + data['DB_ENDPOINT'] + "/" + data['DB_NAME'] +"|g' " + template + "",
                "sudo sed -i 's|SONAR_HOSTS|" + sonar_ips + "|g' " + template + "",
                "sudo sed -i 's|SEARCH_HOSTS|" + search_ips + "|g' " + template + "",
                "sudo sed -i 's|HOSTNAME|" + host + "|g' " + template + "",
                "sudo chmod 755 -R /srv && sudo chown sonar:sonar -R /srv",
                "sudo su sonar /srv/sonarqube/bin/linux-x86-64/sonar.sh start"
                ]
            for command in commands:
                stdin , stdout, stderr = c.exec_command(command)
                print stdout.read()
                print stderr.read()

        return
        {
            'message' : "Script execution completed. See Cloudwatch logs for complete output"
        }
    
    def validate(a, b, c):
        instance_health = a
        instance_status = b
        ec2_status  = c
        print a, b, c

        if (len(set(instance_health)) == 1 and instance_health[0] == 'Healthy'):
            print ('asg_health-ok')
        else:
            print ('asg_health-fail')
        if (len(set(instance_status)) == 1 and instance_status[0] == 'InService'):
            print ('asg_service-ok')
        else:
            print ('asg_service-fail')
        if (len(set(ec2_status)) == 1 and ec2_status[0] == 'running'):
            print ('ec2_running-ok')
        else:
            print ('ec2_running-fail')
        if ((len(set(instance_health)) == 1 and instance_health[0] == 'Healthy') and (len(set(instance_status)) == 1 and instance_status[0] == 'InService') and (len(set(ec2_status)) == 1 and ec2_status[0] == 'running')):
            return ('active')
        else:
            return ('fail')

    status = validate(get_asg_instance_health(), get_asg_instance_status(), get_ec2_status(get_asg_instance_id()))

    while status != 'active':
        print("Inside-Loop")
        time.sleep(5)
        status = validate(get_asg_instance_health(), get_asg_instance_status(), get_ec2_status(get_asg_instance_id()))
        print validate(get_asg_instance_health(), get_asg_instance_status(), get_ec2_status(get_asg_instance_id()))
    else:
        print("outside-loop")
        print validate(get_asg_instance_health(), get_asg_instance_status(), get_ec2_status(get_asg_instance_id()))
        sonar_ips = get_ec2_instance_ips(get_asg_instance_id())
        search_ips = get_search_instance_ips()
        config(sonar_ips, search_ips)